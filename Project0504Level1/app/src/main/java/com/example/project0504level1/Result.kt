package com.example.project0504level1

sealed class Result<out T, in R>() {
     class Success<T, R>(val value: T): Result<T,R>()
     class Error<T, R>(val error: R): Result<T,R>()

    fun getResultsFromDivision(f : Int, s: Int): Result<Int, String> =
        when (s) {
            0 -> Error("We can't use this number in division")
            else -> Success(f / s)
        }

}
