package com.example.project0504level1

class Queue<T> {
    var elements: MutableList<T> = mutableListOf()

    fun enqueue(item: T) = elements.add(item)
    fun dequeue(): T? = if(elements.isNotEmpty()) elements.removeAt(0) else null
}
