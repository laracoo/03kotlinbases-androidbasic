package com.example.project0418level1

interface Warrior {
    var isKilled: Boolean
    val chanceOfMissShot: Int

    fun attack(warrior: Warrior)
    fun damageOfAttack(damage: Int)
}