package com.example.project0317level1

fun main() {
    var list: MutableList<Int>
    var nodList: MutableList<Int>

    print("Введите количество цифр которые вы хотите ввести: ")
    val numberCount = readLine()?.toIntOrNull() ?: return

    list = arrayListCreating(numberCount)
    println("Ваш список: $list")

    println("Количество введенных положительных чисел: ${foundOutPositiveNumbers(list)}")

    println("Все четные числа: ${foundOutEvenNumbers(list)}")

    println("Количество эксклюзивных чисел: ${foundOutExclusiveNumbers(list)}")

    println("Сумма всех чисел введенных вами: ${foundOutSumOfAllNumbers(list)}")

    formattingOfStrings(list)

}

fun arrayListCreating(n: Int): MutableList<Int> {
    var list: MutableList<Int> = mutableListOf()
    var count: Int = 0
    while(count < n) {
        print("Введите число: ")
        val number = readLine()?.toIntOrNull() ?: continue
        list.add(number)
        count++
    }
    return list
}

fun foundOutPositiveNumbers(list: MutableList<Int>): Int {
    var listOfPositiveNumbers: MutableList<Int> = mutableListOf()
    for (num in list) {
        if(num >= 0) {
            listOfPositiveNumbers.add(num)
        }
    }
    return listOfPositiveNumbers.size
}

fun foundOutEvenNumbers(list: MutableList<Int>): List<Int> {
    return list.filter { it % 2 == 0 }
}

fun foundOutExclusiveNumbers(list: MutableList<Int>): Int {
    var set: MutableSet<Int> = mutableSetOf()
    for (num in list) {
        set.add(num)
    }
   return set.size
}

fun foundOutSumOfAllNumbers(list: MutableList<Int>): Int {
    return list.sum()
}

tailrec fun foundOutNODOfNumbers(a: Int, b: Int): Int = if (b == 0) Math.abs(a) else foundOutNODOfNumbers(b,a % b)


fun putIntoHashMap(list: MutableList<Int>): Map<Int,Int> {
    var listOfNods: MutableList<Int> = mutableListOf()
    val map: HashMap<Int, Int> = hashMapOf()
    var mean: Int = 0

    for (num in list) {
       foundOutNODOfNumbers(num, foundOutSumOfAllNumbers(list))
        listOfNods.add(num)
    }
    while(mean < list.size) {
        map[list[mean]] = listOfNods[mean]
        mean ++
    }
    return map
}

fun formattingOfStrings(list: MutableList<Int>) {
    for((k, v) in putIntoHashMap(list)) {
        println("Число<$k> Сумма<${foundOutSumOfAllNumbers(list)}> НОД<$v>")
    }
}