package com.example.project0418level1

import kotlin.random.Random

abstract class AbstractWarrior(val maxHealthLevel: Int, override val chanceOfMissShot: Int, val hitAccuracy: Int, val  abstractWeapon: AbstractWeapon): Warrior {
    var health: Int = maxHealthLevel


    override fun attack(warrior: Warrior) {
        if(abstractWeapon.isPatronsInside) {
            val listAmmo = abstractWeapon.getPatrons()
            var damage = 0

            for(shot in listAmmo.iterator()) {
                if(Random.nextInt(100) > hitAccuracy && Random.nextInt(100) > chanceOfMissShot) {
                    damage += shot.totalDamage()
                }
            }
            warrior.damageOfAttack(damage)
        } else {
            abstractWeapon.restart()
        }
    }
    override fun damageOfAttack(damage: Int) {
        health -= damage
        if(health <= 0) {
            health = 0
            isKilled = true
        }
    }
}