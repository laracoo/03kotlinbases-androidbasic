package com.example.project0504level1

fun main(){
    val listOfInputs: MutableList<Int> = mutableListOf(1,3,5,6,4,3,55,3,22)
    val listOfInputs2: MutableList<Float> = mutableListOf(1f,3f,5f,6.3f,6f,4f,3f,55f,3f,22f)
    val listOfInputs3: MutableList<Double> = mutableListOf(1.0,3.0,5.0,6.0,4.0,3.0,55.0,3.0,22.0)

    fun <T: Number> genericFunction(listOfInputs: MutableList<T>): List<T> {
        return listOfInputs.filter { it.toFloat() % 2 == 0f}
    }
    println("First List : ${genericFunction(listOfInputs)}")
    println("Second List : ${genericFunction(listOfInputs2)}")
    println("Third List : ${genericFunction(listOfInputs3)}")

}



